<?php

/**
 * Fetch the currency exchange rates for the requested currency combination.
 * Use HMRC as a provider.
 *
 * Return an array with the array(target_currency_code => rate) combination.
 *
 * @param string $currency_code
 *   Source currency code.
 * @param array $target_currencies
 *   Array with the target currency codes.
 *
 * @return array
 *   Array with the array(target_currency_code => rate) combination.
 */
function commerce_multicurrency_hmrc_exchange_rate_sync_provider($currency_code, $target_currencies) {
  $data = cache_get(__FUNCTION__, 'cache');
  $date_string = date('my');
  $file_name = "exrates-monthly-$date_string.xml";
  $file_path = 'http://www.hmrc.gov.uk/softwaredevelopers/rates/' . $file_name;

  watchdog('commerce_multicurrency_hmrc', 'Retrieving exchange rates. source: %from, target: %to. from %finename', array( '%from' => $currency_code, '%to' => $target_currencies, '%finename' => $file_name), WATCHDOG_INFO);

  if (!$data) {
    $hmrc_rates = array();
    if (($xml = @simplexml_load_file($file_path)) && @count($xml->exchangeRate)) {


      foreach($xml->exchangeRate as $xmlrate){
        $rate = (float) str_replace(',', '.', (string) $xmlrate->rateNew);
        $code = (string) $xmlrate->currencyCode;
        $hmrc_rates[$code] = $rate;
      }
      cache_set(__FUNCTION__, $hmrc_rates, 'cache', time() + 3600);
    }
    else {
      watchdog(
        'commerce_multicurrency_hmrc', 'Unable to fetch / process the currency data of @url',
        array('@url' => $file_path),
        WATCHDOG_ERROR
      );
    }
  }
  else {
    $hmrc_rates = $data->data;
  }


  $rates = array();
  foreach ($target_currencies as $target_currency_code) {
    if ($currency_code == 'GBP' && isset($hmrc_rates[$target_currency_code])) {
      $rates[$target_currency_code] = $hmrc_rates[$target_currency_code];
    }
    elseif (isset($hmrc_rates[$currency_code]) && $target_currency_code == 'GBP') {
      // Reverse rate calculation
      $rates[$target_currency_code] = 1 / $hmrc_rates[$currency_code];
    }
    elseif (isset($hmrc_rates[$currency_code]) && isset($hmrc_rates[$target_currency_code])) {
      // Cross rate calculation
      $rates[$target_currency_code] = $hmrc_rates[$target_currency_code] / $hmrc_rates[$currency_code];
    }
  }
  return $rates;
}

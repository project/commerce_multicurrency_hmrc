Exchange rates from HMRC for Commerce Multicurrency module.

INSTALLATION
Install the module as usual.

CONFIGURATION
1. Select "HMRC" as sync provider on currency
   conversion settings page: admin/commerce/config/currency/conversion
2. Run cron or sync manually to synchronize the rates.



The module is created and maintained by Guy Schneerson for blue-bag.com